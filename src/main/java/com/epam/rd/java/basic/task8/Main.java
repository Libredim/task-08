package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.students.Student;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.util.Set;

public class Main {
	
	public static void main(String[] args) throws Exception {


		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
//		DOMController domController = new DOMController(xmlFileName);
		StudentsDOMBuilder domBuilder = new StudentsDOMBuilder();
		domBuilder.buildSetStudents(xmlFileName);
		Set<Student> DOMStudents=domBuilder.getStudents();

		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		CreateDocument domDoc=new CreateDocument(outputXmlFile);
		domDoc.create(DOMStudents);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
//		SAXController saxController = new SAXController(xmlFileName);
		StudentsSAXBuilder saxBuilder = new StudentsSAXBuilder();
		saxBuilder.buildSetStudents(xmlFileName);
		Set<Student> SAXstudents=saxBuilder.getStudents();

		// sort  (case 2)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		CreateDocument saxDoc=new CreateDocument(outputXmlFile);
		saxDoc.create(SAXstudents);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		StudentsStAXBuilder staxBuilder = new StudentsStAXBuilder();
		staxBuilder.buildSetStudents(xmlFileName);
		Set<Student> StaxStudents=staxBuilder.getStudents();
		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		CreateDocument staxDoc=new CreateDocument(outputXmlFile);
		staxDoc.create(SAXstudents);
	}

}
