package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.students.Student;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.math.BigInteger;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class StudentsSAXBuilder {
    private Set<Student> students;
    private StudentHandler sh;
    private XMLReader reader;
    public StudentsSAXBuilder() {
// создание SAX-анализатора
        sh = new StudentHandler();
        try {
// создание объекта-обработчика
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(sh);
        } catch (SAXException e) {
            System.err.print("ошибка SAX парсера: " + e);
        }
    }
    public Set<Student> getStudents() {
        return students;
    }
    public void buildSetStudents(String fileName) {
        try {
// разбор XML-документа
            reader.parse(fileName);
        } catch (SAXException e) {
            System.err.print("ошибка SAX парсера: " + e);
        } catch (IOException e) {
            System.err.print("ошибка I/О потока: " + e);
        }
        students = sh.getStudents();
    }

    public enum StudentEnum {
        STUDENTS("students"),
        LOGIN("login"),
        FACULTY("faculty"),
        STUDENT("student"),
        NAME("name"),
        TELEPHONE("telephone"),
        COUNTRY("country"),
        CITY("city"),
        STREET("street"),
        ADDRESS("address");
        private String value;
        private StudentEnum(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }
    public class StudentHandler extends DefaultHandler {
        private Set<Student> students;
        private Student current = null;
        private StudentEnum currentEnum = null;
        private EnumSet<StudentEnum> withText;
        public StudentHandler() {
            students = new HashSet<Student>();
            withText = EnumSet.range(StudentEnum.NAME, StudentEnum.STREET);
        }
        public Set<Student> getStudents() {
            return students;
        }
        public void startElement(String uri, String localName, String qName, Attributes attrs) {
            if ("student".equals(localName)) {
                current = new Student();
                current.setLogin(attrs.getValue(0));
                if (attrs.getLength() == 2) {
                    current.setFaculty(attrs.getValue(1));
                }
            } else {
                StudentEnum temp = StudentEnum.valueOf(localName.toUpperCase());
                if (withText.contains(temp)) {
                    currentEnum = temp;
                }
            }
        }
        public void endElement(String uri, String localName, String qName) {
            if ("student".equals(localName)) {
                students.add(current);
            }
        }
        public void characters(char[] ch, int start, int length) {
            String s = new String(ch, start, length).trim();
            if (currentEnum != null) {
                switch (currentEnum) {
                    case NAME:
                        current.setName(s);
                        break;
                    case TELEPHONE:
                        current.setTelephone(BigInteger.valueOf(Integer.parseInt(s)));
                        break;
                    case STREET:
                        current.getAddress().setStreet(s);
                        break;
                    case CITY:
                        current.getAddress().setCity(s);
                        break;
                    case COUNTRY:
                        current.getAddress().setCountry(s);
                        break;
                    default:
                        throw new EnumConstantNotPresentException(
                                currentEnum.getDeclaringClass(), currentEnum.name());
                }
            }
            currentEnum = null;
        }
    }
}

