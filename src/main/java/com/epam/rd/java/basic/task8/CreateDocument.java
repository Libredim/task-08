package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.students.Address;
import com.epam.rd.java.basic.task8.students.Student;
import com.epam.rd.java.basic.task8.students.Students;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Set;

public class CreateDocument {
    String name;

    CreateDocument(String name) {
        this.name = name;
    }

    void create(Set<Student> students) {
        try {
            JAXBContext context = JAXBContext.newInstance(Students.class);
            Marshaller m = context.createMarshaller();
            Students st = new Students() { // анонимный класс
                {
// добавление первого студента
                    for (Student temp : students) {
                        Student s = temp;
                        this.add(s);
                    }
                }
            };
            m.marshal(st, new FileOutputStream(name));
            m.marshal(st, System.out); // копия на консоль
            System.out.println("XML-файл создан");
        } catch (FileNotFoundException e) {
            System.out.println("XML-файл не может быть создан: " + e);
        } catch (JAXBException e) {
            System.out.println("JAXB-контекст ошибочен " + e);
        }
    }
}

